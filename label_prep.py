import pyspark.sql.functions as funcs
import pyspark.sql.types as types
from pyspark.sql import functions as F
from pyspark.sql import SparkSession

if __name__=='__main__':
    spark  = spark = SparkSession \
    .builder \
    .appName("labelgen") \
    .getOrCreate()
    
    df = spark.read.option("header", "false").option('delimiter','|').csv("s3://ds102-ssagroup-scratch/historical_data_time_2009Q1.txt")
    df = df.select('_c0','_c3')

    def clean(x):
        if x=='XX':
            return 0
        elif x=='R':
            return 0
        elif x == ' ':
            return 0
        elif x == '09':
            return 1
        elif x == '03':
            return 1
        elif x == '06':
            return 1
        x = int(x)
        if x<3:
            return 0
        else:
            return 1

    def classify(x):
        if(x>0.5):
            return 1
        else:
            return 0

    clean_udf = funcs.udf(clean, types.IntegerType())
    transformed_df = df.withColumn('clean', clean_udf('_c3'))
    #transformed_df.limit(4).show()
    new_df = transformed_df.select('_c0','clean')
    groups = new_df.groupBy('_c0').agg(F.mean('clean'))

    classify_udf = funcs.udf(classify, types.IntegerType())
    final_df = groups.withColumn('default', classify_udf('avg(clean)')).select('_c0','default')
    final_df = final_df.selectExpr("_c0 as loan_sequence_number", "default as default")
    final_df.write.format('parquet').mode('overwrite').save('s3://ds102-ssagroup-scratch/label_generation_table')

    spark.stop()


