from dask import delayed
from dask.distributed import Client
import dask.dataframe as dd
import dask.array as da
from dask_ml.preprocessing import Categorizer, DummyEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline


def drop_rows(df):
    df = df[df.credit_score!=9999]
    #df['first_time_homebuyer_flag'] = df['first_time_homebuyer_flag'].mask(df['first_time_homebuyer_flag']==9,'N')
    df = df[df.first_time_homebuyer_flag!='9']
    df['first_time_homebuyer_flag'] = df.first_time_homebuyer_flag=='Y'
    df = df[df.mortgage_insurance_percentage!=999]
    df = df[df.number_of_units!=99]
    df = df[df.occupancy_status!=9]
    df = df[df.channel!=9]
    df['ppm_flag'] = df.ppm_flag=='Y'
    df['amortization_type'] = df.amortization_type=='FRM'
    df = df[df.property_type!=99]
    df = df[df.loan_purpose!=9]
    df = df[df.number_of_borrowers!=99]
    df['super_conforming_flag'] = df.super_conforming_flag=='Y'
    df['io_indicator'] = df.io_indicator=='Y'
    return df


def one_hot_encoder_helper(df,colname):
    pipe = make_pipeline(
    Categorizer(), DummyEncoder())
    input_col  = df[[colname]]
    pipe.fit(input_col)
    final_table =  pipe.transform(input_col)
    return final_table

def standard_scaler_helper(df,colname):
    scaler = StandardScaler()
    input_col = df[[colname]]
    scaler.fit(input_col)
    return scaler.transform(input_col)

def main():
    client = Client()
    # setting up dataframes
    df1 = dd.read_csv('historical_data_2009Q1.txt',sep='|',header=None,dtype={25:'object'})
    df1cols = ['credit_score','first_payment_date','first_time_homebuyer_flag','maturity_date','msa','mortgage_insurance_percentage','number_of_units','occupancy_status','cltv','dti','original_upb','original_ltv','original_interest_rate','channel','ppm_flag','amortization_type','property_state','property_type','postal_code','loan_sequence_number','loan_purpose','original_loan_term','number_of_borrowers','seller_name','servicer_name','super_conforming_flag','preharp_loan_sequence_number','program_indicator','harp_indicator','property_valuation_method','io_indicator']
    df1 = df1.rename(columns=dict(zip(df1.columns, df1cols)))
    
    cols_want = ['credit_score','first_time_homebuyer_flag','mortgage_insurance_percentage',
    'number_of_units','occupancy_status','original_upb','original_interest_rate','channel',
    'ppm_flag','amortization_type','property_type','loan_sequence_number','loan_purpose',
    'number_of_borrowers','super_conforming_flag','io_indicator']
    clean_df1 = drop_rows(df1[cols_want])
    one_hot_cols = ['occupancy_status','channel','property_type','loan_purpose']
    #stdscaler_cols = ['credit_score','mortgage_insurance_percentage','original_upb','original_interest_rate']
    stdscaler_cols = ['credit_score','original_upb']

    one_hot_table = one_hot_encoder_helper(clean_df1,one_hot_cols[0])
    for name in one_hot_cols[1:]:
        add = one_hot_encoder_helper(clean_df1,name)
        one_hot_table = dd.concat([one_hot_table,add],axis=1,interleave_partitions=True)
    
    credit_score_scaled = standard_scaler_helper(clean_df1,stdscaler_cols[0])
    credit_score_scaled = da.from_array(credit_score_scaled, chunks=(100,100))
    original_upb_scaled = standard_scaler_helper(clean_df1,stdscaler_cols[1])
    original_upb_scaled = da.from_array(original_upb_scaled, chunks=(100,100))
    std_scaler_table = dd.concat([dd.from_dask_array(c) for c in [credit_score_scaled,original_upb_scaled]], axis = 1)
    std_scaler_table.columns=['credit_score','original_upb']
    
    #std_scaler_table = std_scaler_table.repartition(npartitions=1)
    #one_hot_scaled = one_hot_table.merge(std_scaler_table)
    org_df = clean_df1[['first_time_homebuyer_flag','mortgage_insurance_percentage','number_of_units','original_interest_rate','ppm_flag','amortization_type','loan_sequence_number','number_of_borrowers','super_conforming_flag','io_indicator']]

    one_hot_table = one_hot_table.reset_index(drop=True)
    
    std_scaler_table = std_scaler_table.repartition(npartitions=2)
    std_scaler_table = std_scaler_table.reset_index(drop=True)
    org_df = org_df.reset_index(drop=True)
    
    #org_hot = dd.concat([org_df,one_hot_table],axis=1,interleave_partitions=True)
    #final_table = dd.concat([org_hot,std_scaler_table],axis=1,interleave_partitions=True)

    hot_org = org_df.merge(one_hot_table,left_index=True,right_index=True)
    final_table = hot_org.merge(std_scaler_table,left_index=True,right_index=True)
    final_table = final_table.drop_duplicates(subset=['loan_sequence_number'])
    final_table.to_parquet("feat_table")

if __name__=='__main__':
    main()
    #print('done')



